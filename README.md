# What was updated
1. Required framework: Java Serenity + Maven
* According to the requirements - removed gradle In the requirements said the project should include Serenity + Maven + Java
2. Refactor the old project
* Refactor/changed Cucumber/Gherkin steps to more comfortable reading code also new code more reusable
```Gherkin
NEW example
@Then("the user should see the response with a {int} status code")
when {int} - can change to different status code according to expected
```
```Gherkin
OLD example
@Then("he sees the results displayed for mango")
not comfortable reusing for new test with another endpoint need to create new method
```
3. Created different folders and files according Page Object Model

 * All data was in two files and one folder : folder stepdefinitions, files CarsAPI.java, SearchStepDefinitions.java
 * Page Object Model - more comfortable for understanding architecture of project
```Gherkin
src
  + test
    + java
      + starter
        + endpoints
        + model
        + steps
        + util
      + resources
        + features
          + search
            getProduct.feature
             
```

4. Cleaning/updated pom.xml
 * Cleaning not usable dependency - as less dependency that better to support project
 * Updated to newest version

5. Configuration project to run test from console by Tags, and custom URL
* Configuration project to run test using Tags and URL from console to run test in parallel
6. Creating new steps
* Added new steps for verification status code - to mace sure that response is correct


