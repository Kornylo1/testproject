Feature: Product Search

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  @smoke @smoke1
  Scenario: sends a GET request to the 'cola' endpoint  with verification provider
    Given the user sends a GET request to the 'cola' endpoint
    When the user should see the response with a 200 status code
    Then the user should see the response with key 'provider[0]' value equal 'Coop'

  @smoke @smoke2
  Scenario: sends a GET request to the 'orange' endpoint with verification provider
    Given the user sends a GET request to the 'orange' endpoint
    When the user should see the response with a 200 status code
    Then the user should see the response with key 'provider[0]' value equal 'pasta'
