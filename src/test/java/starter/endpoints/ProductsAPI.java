package starter.endpoints;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.serenitybdd.rest.SerenityRest;
import starter.model.Product;

import java.io.IOException;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;

public class ProductsAPI {

    /**
     * Method to assert the response status code
     *
     * @param expectedStatusCode = status code what expected
     */
    public void assertResponseStatus(int expectedStatusCode) {
        restAssuredThat(response -> response.statusCode(expectedStatusCode));
    }


    /**
     * Method to assert the response body equal a specific value
     *
     * @param key = Path to the value
     * @param expectedValue = value what expected
     */
    public void assertResponseBodyEqual(String key, String expectedValue) {
        restAssuredThat(response -> response.body(key, equalTo(expectedValue)));
    }
    /**
     * Method to assert the response body equal a specific value
     *
     * @param key = Path to the value
     * @param notExpectedValue = value what not expected
     */
    public void assertResponseBodyNotEqual(String key, String notExpectedValue) {
        restAssuredThat(response -> response.body(key, not(equalTo(notExpectedValue))));
    }

    /**
     * Implementation module list of Product objects representing the JSON data
     */
    public void getListOfProduct(){
        String response = SerenityRest.then().extract().body().asString();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Product> products = objectMapper.readValue(response, new TypeReference<List<Product>>() {});
            for (Product product : products) {
                System.out.println("Provider: " + product.getProvider());
                System.out.println("Title: " + product.getTitle());
                System.out.println("url: " + product.getUrl());
                System.out.println("unit: " + product.getUnit());
                System.out.println("isPromo: " + product.isPromo());
                System.out.println("promoDetails: " + product.getPromoDetails());
                System.out.println("image: " + product.getImage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
