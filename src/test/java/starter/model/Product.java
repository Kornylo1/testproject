package starter.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {
    @JsonProperty("provider")
    private String provider;

    @JsonProperty("title")
    private String title;

    @JsonProperty("url")
    private String url;

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("price")
    private double price;

    @JsonProperty("unit")
    private String unit;

    @JsonProperty("isPromo")
    private boolean isPromo;

    @JsonProperty("promoDetails")
    private String promoDetails;

    @JsonProperty("image")
    private String image;

    // Constructors, getters, setters, and other methods...

    // Add constructors, getters, setters, and other methods as needed
}