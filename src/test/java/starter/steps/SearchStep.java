package starter.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.endpoints.ProductsAPI;

import static starter.util.ConstantsAPI.PRODUCT_SEARCH;
import static starter.util.ConstantsAPI.baseURL;


public class SearchStep {

    @Steps
    private ProductsAPI productsAPI;

    @When("the user sends a GET request to the {string} endpoint")
    public void sendGetRequest(String endpoint) {
        SerenityRest.given().baseUri(baseURL).basePath(PRODUCT_SEARCH).get(endpoint);
    }

    @Then("the user should see the response with a {int} status code")
    public void verifyStatusCode(int statusCode) {
        productsAPI.assertResponseStatus(statusCode);
    }

    @Then("the user should see the response with key {string} value equal {string}")
    public void verifyResponseContains(String key, String expectedValue) {
        productsAPI.assertResponseBodyEqual(key, expectedValue);
    }
    @Then("the user should see the response with key {string} value not equal {string}")
    public void verifyResponseNotEqual(String key, String expectedValue) {
        productsAPI.assertResponseBodyNotEqual(key, expectedValue);
    }
}
