package starter.util;

public class ConstantsAPI {
    // This constant represents the base URL for API requests.
    // It is obtained from the serenity conf "restapi.baseurl."
    public static String baseURL = System.getProperty("restapi.baseurl");

    // This constant represents the endpoint for product search.
    public static final String PRODUCT_SEARCH = "/api/v1/search/demo/";

}